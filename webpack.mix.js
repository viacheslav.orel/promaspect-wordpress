const mix = require("laravel-mix");
require("@tinypixelco/laravel-mix-wp-blocks");

mix.autoload({
	jquery: ["$", "window.jQuery", "jQuery"],
});

mix.setPublicPath("./assets/dist");

mix
	.js("assets/src/scripts/app.js", "assets/dist/js")
	.block("assets/src/scripts/gutenberg.js", "assets/dist/js")
	.sass("assets/src/scss/style.scss", "assets/dist/css")
	.sass("assets/src/scss/gutenberg-edit.scss", "assets/dist/css")
	.sass("assets/src/scss/gutenberg.scss", "assets/dist/css");

if (mix.inProduction()) {
	mix.version();
}
