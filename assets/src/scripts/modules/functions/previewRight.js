import { InnerBlocks } from "@wordpress/block-editor";

import { renderImage } from "../functions/renderImage";

export default function ({ attributes }) {
	return (
		<div className="content-preview preview-right">
			<div className="content-preview__image--wrapper">
				<div className="content-preview__image--container">
					{renderImage(
						attributes.imageUrl,
						attributes.imageAlt,
						"content-preview__image"
					)}
				</div>
			</div>
			<div className="content-preview__body">
				<InnerBlocks.Content />
			</div>
		</div>
	);
}
