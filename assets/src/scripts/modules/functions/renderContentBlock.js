import { InnerBlocks } from "@wordpress/block-editor";

export default function contentBlock({attributes}) {
	return (
		<div className="content-block--container">
			<div className="content-block">
				<h2 className="content-block__title">{attributes.title}</h2>
				<div className="content-block__content">
					<InnerBlocks.Content />
				</div>
			</div>
		</div>
	)
}


