import { renderImage } from "../functions/renderImage";

export default function ({attributes}) {
	return (
		<div className="card-industry">
			<div className="card-industry__title--wrapper">
				<h3 className="card-industry__title">{attributes.title}</h3>
			</div>
			<div className="card-industry__image--wrapper">
				{renderImage(
					attributes.imageUrl,
					attributes.imageAlt,
					"card-industry__image"
				)}
			</div>
		</div>
	)
}
