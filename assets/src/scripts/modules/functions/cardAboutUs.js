import { InnerBlocks } from "@wordpress/block-editor";

export default function ({ attributes }) {
	return (
		<div className="card-aboutus">
			<h3 className="card-aboutus__title--container">
				<p className="card-aboutus__title--prefix">{attributes.titlePrefix}</p>
				<p className="card-aboutus__title">{attributes.title}</p>
			</h3>
			<div className="card-aboutus__body">
				<InnerBlocks.Content />
			</div>
		</div>
	);
}
