export function renderImage(src, alt, className, imageWidth, imageHeight) {
	if (!src) return null;

	let attribute = "";

	if (imageWidth) {
		attribute = `width="${imageWidth}" height="${imageHeight}"`;
	}

	if (alt) {
		return (
			<img
				className={className}
				src={src}
				alt={alt}
				{...attribute}
			/>
		);
	}

	return (
		<img
			className={className}
			src={src}
			alt=""
			aria-hidden="true"
		/>
	)
}
