import { InnerBlocks } from "@wordpress/block-editor";
import { forwardRef } from "@wordpress/element";

import ImagePicker from "./imagePicker";

function PreviewLeft({ attributes, className, setAttributes }) {
	return (
		<div className="content-preview preview-left">
			<ImagePicker
				imageUrl={attributes.imageUrl}
				containerClass="content-preview__image--container"
				imgClass="content-preview__image"
				setAttributes={setAttributes}
			/>
			<div className="content-preview__body">
				<InnerBlocks allowedBlocks={["core/paragraph", "core/button"]} />
			</div>
		</div>
	);
}

export default forwardRef(PreviewLeft);
