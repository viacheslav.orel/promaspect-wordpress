import { forwardRef } from "@wordpress/element";
import { MediaUpload } from "@wordpress/block-editor";
import { Button } from "@wordpress/components";

function ImagePicker({ imageUrl, containerClass, imgClass, setAttributes, imageWidth = "", imageHeight = "" }) {
	const getImageButton = (openEvent) => {
		if (imageUrl) {
			let attribute = "";

			if (imageHeight) {
				attribute += `height = "${imageHeight}"`;
			}
			
			if (imageWidth) {
				attribute += ` width = "${imageWidth}"`;
			}

			return (
				<div className={containerClass} {...attribute}>
					<img src={imageUrl} onClick={openEvent} className={imgClass} />
				</div>
			);
		} else {
			return (
				<div className="button-container">
					<Button onClick={openEvent} className="button button-large">
						Pick an image
					</Button>
				</div>
			);
		}
	};

	return (
		<MediaUpload
			onSelect={(media) => {
				setAttributes({ imageAlt: media.alt, imageUrl: media.url });
			}}
			type="image"
			render={({ open }) => getImageButton(open)}
		/>
	);
}

export default forwardRef(ImagePicker);
