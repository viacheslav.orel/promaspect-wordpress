import { InnerBlocks } from "@wordpress/block-editor";
import { forwardRef } from "@wordpress/element";

import ImagePicker from "./imagePicker";

function PreviewRight({ attributes, className, setAttributes }) {
	return (
		<div className="content-preview preview-right">
			<div className="content-preview__image--wrapper">
				<ImagePicker
					imageUrl={attributes.imageUrl}
					containerClass="content-preview__image--container"
					imgClass="content-preview__image"
					setAttributes={setAttributes}
				/>
			</div>
			<div className="content-preview__body">
				<InnerBlocks allowedBlocks={["core/paragraph", "core/button"]} />
			</div>
		</div>
	);
}

export default forwardRef(PreviewRight);
