import { PlainText } from "@wordpress/block-editor";
import { forwardRef, Fragment } from "@wordpress/element";

import ImagePicker from "./imagePicker";

function CardIndustry({ attributes, className, setAttributes }) {
	return (
		<Fragment>
			<div className="card-industry__title--wrapper">
				<PlainText
					value={attributes.title}
					placeholder="Write title"
					className="card-industry__title"
					onChange={(content) => setAttributes({ title: content })}
				/>
			</div>
			<ImagePicker
				imageUrl={attributes.imageUrl}
				containerClass="card-industry__image--wrapper"
				imgClass="card-industry__image"
				setAttributes={setAttributes}
			/>
		</Fragment>
	);
}

export default forwardRef(CardIndustry);
