import { forwardRef, Fragment } from "@wordpress/element";
import { InnerBlocks, PlainText } from "@wordpress/block-editor";

function CardAboutUs({ attributes, className, setAttributes }) {
	return (
		<Fragment>
			<div className="card-aboutus__title--container">
				<PlainText
					value={attributes.titlePrefix}
					placeholder="Write title"
					className="card-aboutus__title--prefix"
					onChange={(content) => setAttributes({ titlePrefix: content })}
				/>
				<PlainText
					value={attributes.title}
					placeholder="Write title"
					className="card-aboutus__title"
					onChange={(content) => setAttributes({ title: content })}
				/>
			</div>
			<div className="card-aboutus__body">
				<InnerBlocks allowedBlocks={["core/paragraph"]} />
			</div>
		</Fragment>
	);
}

export default forwardRef(CardAboutUs);
