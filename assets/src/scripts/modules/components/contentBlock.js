import { PlainText, InnerBlocks } from "@wordpress/block-editor";
import { forwardRef } from "@wordpress/element";

function ContentBlock({ attributes, className, setAttributes }) {
	return (
		<div className="content-block">
			<PlainText
				value={attributes.title}
				placeholder="Write title"
				className="content-block__title"
				onChange={(content) => setAttributes({ title: content })}
			/>
			<InnerBlocks
				allowedBlocks={[
					"orslavteam/preview-left",
					"orslavteam/preview-right",
					"orslavteam/cards-industry-container",
					"orslavteam/card-about-us-container",
				]}
			/>
		</div>
	);
}

export default forwardRef(ContentBlock);
