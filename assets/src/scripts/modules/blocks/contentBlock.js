import { registerBlockType } from "@wordpress/blocks";
import ContentBlock from "../components/contentBlock";
import contentBlock from "../functions/renderContentBlock";

registerBlockType("orslavteam/content-block", {
	title: "Content Block",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<rect width="100" height="19" fill="black" />
			<rect x="0.5" y="19.5" width="99" height="80" fill="white" stroke="black" />
		</svg>
	),
	category: "promaspect",
	attributes: {
		title: {
			source: "text",
			selector: "h2.content-block__title"
		},
		body: {
			type: "array",
			source: "children",
			selector: ".content-block__content"
		}
	},
	edit: props => <ContentBlock {...props} />,
	save: props => contentBlock(props)
});
