import { registerBlockType } from "@wordpress/blocks";

import ArticleAccent from '../components/articleAccent';
import renderArticle from '../functions/articleaAccent';

registerBlockType("orslavteam/article-accent", {
	title: "Article Accent",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<rect width="100" height="100" fill="black" />
			<rect y="7" width="66" height="15" fill="white" />
			<rect y="32" width="66" height="10" fill="white" />
			<rect y="45" width="66" height="10" fill="white" />
			<rect y="58" width="66" height="10" fill="white" />
			<rect y="71" width="66" height="10" fill="white" />
			<rect y="84" width="66" height="10" fill="white" />
			<rect x="67" y="7" width="33" height="87" fill="white" />
			<line
				x1="66.5325"
				y1="93.8227"
				x2="99.5325"
				y2="6.82267"
				stroke="black"
			/>
			<line
				x1="99.5325"
				y1="94.1773"
				x2="66.5325"
				y2="7.17733"
				stroke="black"
			/>
		</svg>
	),
	category: "promaspect",
	attributes: {
		title: {
			source: "text",
			selector: "h2.article-accent__title",
			default: "title"
		},
		body: {
			type: "array",
			source: "children",
			selector: ".article-accent__body"
		},
		linkText: {
			type: "text",
			selector: ".article-accent__link--text",
			default: ""
		},
		linkUrl: {
			type: "text",
			source: "attribute",
			selector: "a.article-accent__link",
			attribute: "href",
			default: "#"
		},
		imageUrl: {
			type: "string",
			source: "attribute",
			selector: "img.article-accent__preview",
			attribute: "src"
		},
		imageAlt: {
			type: "string",
			source: "attribute",
			selector: "img.article-accent__preview",
			attribute: "alt"
		},
	},
	edit: props => <ArticleAccent {...props} />,
	save: props => renderArticle(props)
});
