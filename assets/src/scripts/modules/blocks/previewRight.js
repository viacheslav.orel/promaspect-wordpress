import { registerBlockType } from "@wordpress/blocks";

import PreviewRight from "../components/previewRight";
import renderPreviewRight from "../functions/previewRight";

registerBlockType("orslavteam/preview-right", {
	title: "Preview Right",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<rect width="63" height="19" fill="black" />
			<rect y="27" width="63" height="19" fill="black" />
			<rect y="54" width="63" height="19" fill="black" />
			<rect y="81" width="63" height="19" fill="black" />
			<rect
				x="73.5"
				y="0.5"
				width="26"
				height="99"
				fill="white"
				stroke="black"
			/>
			<line
				x1="73.5161"
				y1="99.8742"
				x2="99.5161"
				y2="-0.125815"
				stroke="black"
			/>
		</svg>
	),
	category: "promaspect",
	attributes: {
		imageUrl: {
			type: "string",
			source: "attribute",
			selector: "img.content-preview__image",
			attribute: "src",
		},
		imageAlt: {
			type: "string",
			source: "attribute",
			selector: "img.content-preview__image",
			attribute: "alt",
		},
		content: {
			type: "array",
			source: "children",
			selector: ".content-preview__body",
		},
	},
	edit: (props) => <PreviewRight {...props} />,
	save: (props) => renderPreviewRight(props),
});
