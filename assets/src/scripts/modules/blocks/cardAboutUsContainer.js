import { registerBlockType } from "@wordpress/blocks";
import { InnerBlocks } from "@wordpress/block-editor";

registerBlockType("orslavteam/card-about-us-container", {
	title: "Card About Us Container",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<rect x="12" width="33" height="19" fill="black" />
			<rect x="67" width="33" height="19" fill="black" />
			<rect y="26" width="45" height="11" fill="black" />
			<rect x="55" y="26" width="45" height="11" fill="black" />
			<rect y="42" width="45" height="11" fill="black" />
			<rect x="55" y="42" width="45" height="11" fill="black" />
			<rect y="58" width="45" height="11" fill="black" />
			<rect x="55" y="58" width="45" height="11" fill="black" />
			<rect y="74" width="45" height="11" fill="black" />
			<rect x="55" y="74" width="45" height="11" fill="black" />
			<rect y="90" width="45" height="11" fill="black" />
			<rect x="55" y="90" width="45" height="11" fill="black" />
			<rect width="13" height="19" fill="#FFCC19" />
			<rect x="55" width="13" height="19" fill="#FFCC19" />
		</svg>
	),
	category: "promaspect",
	attributes: {
		cards: {
			type: "array",
			source: "children",
			selector: ".card-aboutus--container",
		},
	},
	edit: ({ attributes, className, setAttributes }) => <InnerBlocks allowedBlocks={["orslavteam/card-about-us"]} />,
	save: ({attributes}) => (
		<div className="card-aboutus--container">
			<InnerBlocks.Content />
		</div>
	)
});
