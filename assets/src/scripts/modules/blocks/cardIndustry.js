import { registerBlockType } from "@wordpress/blocks";

import CardIndusty from "../components/cardIndustry";
import renderCardIndustry from "../functions/cardIndustry";

registerBlockType("orslavteam/card-industry", {
	title: "Card Industry",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<g clip-path="url(#clip0)">
				<rect width="100" height="100" fill="white" />
				<rect width="100" height="19" fill="black" />
				<rect
					x="0.5"
					y="19.5"
					width="99"
					height="80"
					fill="white"
					stroke="black"
				/>
				<line
					x1="-0.314711"
					y1="99.6115"
					x2="99.6853"
					y2="18.6115"
					stroke="black"
				/>
				<line
					x1="0.316619"
					y1="18.613"
					x2="99.3166"
					y2="99.613"
					stroke="black"
				/>
				<path d="M3 15H73M7.5 6V88" stroke="#FFCC19" />
			</g>
			<defs>
				<clipPath id="clip0">
					<rect width="100" height="100" fill="white" />
				</clipPath>
			</defs>
		</svg>
	),
	category: "promaspect",
	attributes: {
		title: {
			source: "text",
			selector: "h3.card-industry__title"
		},
		imageUrl: {
			type: "string",
			source: "attribute",
			selector: "img.card-industry__image",
			attribute: "src"
		},
		imageAlt: {
			type: "string",
			source: "attribute",
			selector: "img.card-industry__image",
			attribute: "alt"
		},
	},
	edit: props => <CardIndusty {...props} />,
	save: props => renderCardIndustry(props)
});
