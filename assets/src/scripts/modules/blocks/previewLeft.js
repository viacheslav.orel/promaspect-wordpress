import { registerBlockType } from "@wordpress/blocks";

import PreviewLeft from "../components/previewLeft";
import renderPreviewLeft from "../functions/previewLeft";

registerBlockType("orslavteam/preview-left", {
	title: "Preview Left",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<g clip-path="url(#clip0)">
				<rect width="100" height="100" fill="white" />
				<rect x="37" width="63" height="19" fill="black" />
				<rect x="37" y="27" width="63" height="19" fill="black" />
				<rect x="37" y="54" width="63" height="19" fill="black" />
				<rect x="37" y="81" width="63" height="19" fill="black" />
				<rect x="0.5" y="0.5" width="26" height="99" stroke="white" />
				<line
					x1="0.516089"
					y1="99.8742"
					x2="26.5161"
					y2="-0.125815"
					stroke="black"
				/>
				<line
					x1="0.483911"
					y1="-0.125817"
					x2="26.4839"
					y2="99.8742"
					stroke="black"
				/>
			</g>
			<defs>
				<clipPath id="clip0">
					<rect width="100" height="100" fill="white" />
				</clipPath>
			</defs>
		</svg>
	),
	category: "promaspect",
	attributes: {
		imageUrl: {
			type: "string",
			source: "attribute",
			selector: "img.content-preview__image",
			attribute: "src"
		},
		imageAlt: {
			type: "string",
			source: "attribute",
			selector: "img.content-preview__image",
			attribute: "alt"
		},
		content: {
			type: "array",
			source: "children",
			selector: ".content-preview__body"
		}
	},
	edit: props => <PreviewLeft {...props} />,
	save: props => renderPreviewLeft(props)
});
