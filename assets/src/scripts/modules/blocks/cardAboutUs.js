import { registerBlockType } from "@wordpress/blocks";

import CardAboutUs from "../components/cardAboutUs";
import renderCard from "../functions/cardAboutUs";

registerBlockType("orslavteam/card-about-us", {
	title: "Card About Us",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<g clip-path="url(#clip0)">
				<rect width="100" height="100" fill="white" />
				<rect x="27" width="73" height="19" fill="black" />
				<rect y="26" width="100" height="11" fill="black" />
				<rect y="42" width="100" height="11" fill="black" />
				<rect y="58" width="100" height="11" fill="black" />
				<rect y="74" width="100" height="11" fill="black" />
				<rect y="90" width="100" height="11" fill="black" />
				<rect width="28" height="19" fill="#FFCC19" />
			</g>
			<defs>
				<clipPath id="clip0">
					<rect width="100" height="100" fill="white" />
				</clipPath>
			</defs>
		</svg>
	),
	category: "promaspect",
	attributes: {
		titlePrefix: {
			source: "text",
			selector: "p.card-aboutus__title--prefix",
			default: "00"
		},
		title: {
			source: "text",
			selector: "p.card-aboutus__title",
			default: "Title"
		},
		body: {
			type: "array",
			source: "children",
			selector: ".card-aboutus__body"
		}
	},
	edit: props => <CardAboutUs {...props} />,
	save: props => renderCard(props)
});
