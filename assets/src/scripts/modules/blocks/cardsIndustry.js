import { registerBlockType } from "@wordpress/blocks";
import { InnerBlocks } from "@wordpress/block-editor";

registerBlockType("orslavteam/cards-industry-container", {
	title: "Cards Industry",
	icon: (
		<svg
			width="100"
			height="100"
			viewBox="0 0 100 100"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<g clip-path="url(#clip0)">
				<rect width="100" height="100" fill="white" />
				<rect width="27" height="19" fill="black" />
				<rect x="36" width="27" height="19" fill="black" />
				<rect x="73" width="27" height="19" fill="black" />
				<rect
					x="0.5"
					y="19.5"
					width="26"
					height="80"
					fill="white"
					stroke="black"
				/>
				<rect
					x="36.5"
					y="19.5"
					width="26"
					height="80"
					fill="white"
					stroke="black"
				/>
				<rect
					x="73.5"
					y="19.5"
					width="26"
					height="80"
					fill="white"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.3815"
					y2="-0.5"
					transform="matrix(0.316228 -0.948683 0.21365 0.97691 0 100)"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.3815"
					y2="-0.5"
					transform="matrix(0.316228 -0.948683 0.21365 0.97691 36 100)"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.3815"
					y2="-0.5"
					transform="matrix(0.316228 -0.948683 0.21365 0.97691 73 100)"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.2965"
					y2="-0.5"
					transform="matrix(0.313377 0.949629 -0.215708 0.976458 0 19)"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.2965"
					y2="-0.5"
					transform="matrix(0.313377 0.949629 -0.215708 0.976458 36 19)"
					stroke="black"
				/>
				<line
					y1="-0.5"
					x2="85.2965"
					y2="-0.5"
					transform="matrix(0.313377 0.949629 -0.215708 0.976458 73 19)"
					stroke="black"
				/>
				<path d="M1 15H20M2.22143 6V88" stroke="#FFCC19" />
				<path d="M37 15H56M38.2214 6V88" stroke="#FFCC19" />
				<path d="M74 15H93M75.2214 6V88" stroke="#FFCC19" />
			</g>
			<defs>
				<clipPath id="clip0">
					<rect width="100" height="100" fill="white" />
				</clipPath>
			</defs>
		</svg>
	),
	category: "promaspect",
	attributes: {
		cards: {
			type: "array",
			source: "children",
			selector: ".cards-industry--container"
		}
	},
	edit: ({ attributes, className, setAttributes }) => <InnerBlocks allowedBlocks={["orslavteam/card-industry"]} />,
	save: ({attributes}) => (
		<div className="cards-industry--container">
			<InnerBlocks.Content />
		</div>
	)
});
