import { registerBlockStyle, unregisterBlockStyle } from "@wordpress/blocks";


unregisterBlockStyle(
	'core/button',
	['default', 'outline', 'squared', 'fill']
)

registerBlockStyle(
	'core/button',
	[
		{
			name: 'default',
			label: 'Default',
			isDefault: true,
		},
		{
			name: 'content-preview__link',
			label: 'Previe Link'
		}
	]
)
