import $ from 'jquery';

$(document).ready(() => {
	const $dropdownComponens = $('[data-type="dropdown"]');
  
	$dropdownComponens.click((event) => {
	  const $dropdown = $(event.currentTarget).parent();
  
	  if ($dropdown.hasClass('lang--dropdown-open')) {
		$dropdown.removeClass('lang--dropdown-open');
	  } else {
		$dropdown.addClass('lang--dropdown-open');
	  }
	});
  
	const $modalBtns = $('[data-type="modal"]');
	const $modalCloseBtns = $('[data-type="modal-close"]');
	const $body = $('body');
  
	let $modal;
  
	$modalBtns.click((event) => {
	  const $btn = $(event.currentTarget);
	  $modal = $(`#${$btn.attr('data-target')}`);
  
	  $modal.removeAttr('hidden');
	  $body.addClass('modal-open');
	});
  
	$modalCloseBtns.click(() => {
	  $body.removeClass('modal-open');
	  $modal.attr('hidden', 'hidden');
	});
  });
  