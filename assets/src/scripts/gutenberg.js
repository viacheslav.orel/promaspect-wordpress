require('./modules/blocks/contentBlock');
require('./modules/blocks/previewLeft');
require('./modules/blocks/previewRight');

require('./modules/blocks/cardIndustry');
require('./modules/blocks/cardsIndustry');

require('./modules/blocks/cardAboutUsContainer');
require('./modules/blocks/cardAboutUs');

require('./modules/blocks/articleAccent');

require('./modules/blocks/button');
