<?php
/**
 * Template header part for displaying posts
 *
 * Authod: Viacheslav Orel
 *
 * @package promaspect
 */

?>

<div class="page-header">
	<div class="company">
		<div class="company__name-wrapper">
			<h1 class="company__name"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></h1>
		</div>
		<div class="company__description-wrapper">
			<p class="company__description"><?php echo esc_html( get_bloginfo( 'description' ) ); ?></p>
		</div>
	</div>
	<div class="page-header__preview-wrapper">
		<img class="page-header__preview" src="<?php echo esc_html( get_field( 'header_image' )['url'] ); ?>" alt="" />
		<div class="page-header__social-wrapper">
			<div class="page-header__social">
				<div class="social__item-container">
					<a class="social__item" href="https://instagram/" target="_blank">istagram</a>
				</div>
				<div class="social__item-container">
					<a class="social__item" href="https://uk-ua.facebook.com/" target="_blank">facebook</a>
				</div>
				<div class="social__item-container">
					<a class="social__item" href="https://linkedin/" target="_blank">linkedin</a>
				</div>
			</div>
		</div>
	</div>
</div>
