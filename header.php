<?php
/**
 * The template for displaying the header
 *
 * Author: Viacheslav Orel
 *
 * @package promaspect
 */

use lloc\Msls\MslsBlogCollection;

$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo           = wp_get_attachment_image_src( $custom_logo_id, 'full' );
$blog           = MslsBlogCollection::instance()->get_current_blog();
$current_lang   = $blog->get_description();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
</head>

<?php wp_body_open(); ?>
<header class="header header-home">
	<div class="container-fluid navbar-wrapper">
		<div class="container navbar">
			<div class="lang--dropdown">
				<p class="lang--dropdown-selected" data-type="dropdown">
					<span class="lang--dropdown-item"><?php echo esc_html( $current_lang ); ?></span>
					<svg class="lang--dropdown-symbol" width="10" height="6" viewBox="0 0 10 6" fill="none"
						xmlns="http://www.w3.org/2000/svg">
						<path d="M1 1L5 5L9 1" stroke="white" />
					</svg>
				</p>
				<div class="lang--dropdown-list">
					<?php foreach ( lloc\Msls\MslsOutput::init()->get( 1 ) as $lang ) : ?>
						<?php echo $lang; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="logo-wrapper">
				<a class="logo-container" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img class="logo" src="<?php echo esc_url( $logo[0] ); ?>" alt="" />
				</a>
			</div>
			<?php
				wp_nav_menu(
					array(
						'container'      => 'nav',
						'menu_class'     => 'menu main_menu',
						'theme_location' => 'primary',
					)
				)
				?>
			<div class="lang">
				<div class="lang--item-container">
					<p class="current_language"><?php echo esc_html( $current_lang ); ?></p>
				</div>
				<?php foreach ( lloc\Msls\MslsOutput::init()->get( 1 ) as $lang ) : ?>
				<div class="lang--item-container">
					<?php echo $lang; ?>
				</div>
				<?php endforeach; ?>
			</div>
			<button class="burger-btn" data-target="navbarMobile" data-type="modal">
				<img src="<?php echo esc_url( get_template_directory_uri() . '/img/hamburgerMenu.svg' ); ?>" alt="" />
			</button>
		</div>
	</div>
	<div class="navbar--modal" id="navbarMobile" hidden>
		<div class="navbar--modal-header">
			<p class="navbar--modal-title">PROMASPECT</p>
			<button class="burger-btn" data-type="modal-close">
				<img src="<?php echo esc_url( get_template_directory_uri() . '/img/closeHamburgerMenu.svg' ); ?>"
					alt="" />
			</button>
		</div>
		<div class="navbar--modal-body">
			<?php
				wp_nav_menu(
					array(
						'container'      => 'nav',
						'menu_class'     => 'menu min_menu',
						'theme_location' => 'primary',
					)
				)
				?>
			<div class="modal-social">
				<div class="modal-social--container">
					<div class="modal-social--item-container">
						<a class="modal-social--item" href="https://instagram/" target="_blank">istagram</a>
					</div>
					<div class="modal-social--item-container">
						<a class="modal-social--item" href="https://uk-ua.facebook.com/" target="_blank">facebook</a>
					</div>
					<div class="modal-social--item-container">
						<a class="modal-social--item" href="https://linkedin/" target="_blank">linkedin</a>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar--modal-footer">
			<div class="modal-lang">
				<div class="modal-lang--item-container">
					<p class="modal-lang--item"><?php echo esc_html( $current_lang ); ?></p>
				</div>
				<?php foreach ( lloc\Msls\MslsOutput::init()->get( 1 ) as $lang ) : ?>
				<div class="modal-lang--item-container">
					<?php echo $lang; ?>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php
	if ( is_front_page() ) {
		get_template_part( 'page-template/content', 'home-header' );
	}
	?>
</header>
