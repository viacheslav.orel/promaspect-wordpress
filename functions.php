<?php
/**
 * Author: Viacheslav Orel
 *
 * @package promaspect
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @return void
 */
function promaspect_script_enqueue() {
	wp_enqueue_style( 'customstyle-css', get_template_directory_uri() . '/assets/dist/css/style.css', array(), '1.0.0', 'all' );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/dist/js/app.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'promaspect_script_enqueue' );

/**
 * Registers support for various WordPress features.
 *
 * @return void
 */
function promaspect_theme_setup() {
	add_theme_support( 'menus' );
	register_nav_menus( array( 'primary' => 'Primary Header Navigation' ) );
}
add_action( 'init', 'promaspect_theme_setup' );


add_theme_support(
	'custom-logo',
	array(
		'flex-width'  => true,
		'flex-height' => true,
	)
);

/**
 * Register promaspect category for Gutenberg
 *
 * @param array   $categories Array of block categories.
 * @param WP_Post $post Post being loaded.
 * @return array
 */
function orslavteam_promaspect_gutenberg_category( $categories, $post ) {

	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'promaspect',
				'title' => 'promaspect',
			),
		)
	);
}
add_filter( 'block_categories', 'orslavteam_promaspect_gutenberg_category', 10, 2 );

/**
 * Undocumented function
 *
 * @return void
 */
function orslavteam_promaspect_gutenberg_blocks_assets() {
	wp_register_style(
		'orslavteam-promaspect-gutenberg-style',
		get_template_directory_uri() . '/assets/dist/css/gutenberg.css',
		array(),
		'1.0.0'
	);
	wp_register_script(
		'orslavteam-promaspect-gutenberg-script',
		get_template_directory_uri() . '/assets/dist/js/gutenberg.js',
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
		'1.0.0',
		true
	);
	wp_register_style(
		'orslavteam-promaspect-gutenberg-style-editor',
		get_template_directory_uri() . '/assets/dist/css/gutenberg-edit.css',
		array(),
		'1.0.0'
	);

	register_block_type(
		'orslavteam/promaspect-gutenberg',
		array(
			'style'         => 'orslavteam-promaspect-gutenberg-style',
			'editor_style'  => 'orslavteam-promaspect-gutenberg-style-editor',
			'editor_script' => 'orslavteam-promaspect-gutenberg-script',
		)
	);
}
add_action( 'init', 'orslavteam_promaspect_gutenberg_blocks_assets', 99 );

function smartwp_remove_wp_block_library_css() {
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
