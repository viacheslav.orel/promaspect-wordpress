<?php
/**
 * The template for displaying only Gutenberg content
 *
 * Template Name: Langding Page
 * Author: Viacheslav Orel
 *
 * @package promaspect
 */

get_header();
?>

<main class="main">
	<?php the_content(); ?>
</main>

<?php
get_footer();
